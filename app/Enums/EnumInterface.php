<?php

namespace App\Enums;


interface EnumInterface
{
    public static function getDescription(string $value);
}
