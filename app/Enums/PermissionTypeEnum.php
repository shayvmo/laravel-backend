<?php

namespace App\Enums;

final class PermissionTypeEnum implements EnumInterface
{
    // 菜单
    public const MENU =   1;

    // 按钮
    public const BUTTON =   2;

    // 接口
    public const API = 3;

    public static function getDescription(string $value)
    {
        $desc = [
            self::MENU => '菜单',
            self::BUTTON => '按钮',
            self::API => '接口',
        ];

        return $desc[$value] ?? $value;
    }

}
