<?php

namespace App\Enums;


class FileChannelEnum implements EnumInterface
{
    public const ADMIN = 1;

    public const CLIENT = 2;

    public static function getDescription(string $value)
    {
        $desc = [
            self::ADMIN => '后台端',
            self::CLIENT => '用户端',
        ];

        return $desc[$value] ?? $value;
    }
}
