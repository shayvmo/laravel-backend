<?php

namespace App\Enums;


final class SettingEnum implements EnumInterface
{
    public const STORAGE =  'storage';

    public static function getDescription($value): string
    {
        $desc = [
            self::STORAGE => '上传配置',
        ];

        return $desc[$value] ?? $value;
    }


}
