<?php
/**
 * eshop
 *
 * @ClassName RepositoryInterface
 * @Author Eric
 * @Date 2021-07-31 10:23 星期六
 * @Version 1.0
 * @Description
 */


namespace App\Repositories\Core;


interface RepositoryInterface
{
    public function all($columns = array('*'));
    public function paginate($perPage = 15, $columns = array('*'));
    public function create(array $data);
    public function update(array $data, $id);
    public function delete($id);
    public function find($id, $columns = array('*'));
    public function findBy($field, $value, $columns = array('*'));
}
