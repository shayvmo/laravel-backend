<?php


namespace App\Http\Controllers\Frontend\v1;


use App\Http\Controllers\Frontend\BaseController;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class DemoController extends BaseController
{
    /**
     * @Description
     * @Author shayvmo
     * @Date 2021/4/13 16:59
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $user = User::query()->find(1);
        $token = auth('api')->user();
//        $token = $user->generateToken();
        return $this->success('前台demo', compact('token'));
    }

    public function login()
    {
        $user = User::query()->find(1);
        $token = $user->generateToken();
        return $this->success('登录成功', compact('token'));
    }

    public function test()
    {
        return $this->success('登录用户: '.Auth::id());
    }
}
