<?php

namespace App\Http\Controllers\Frontend;


use App\Http\Controllers\Controller;

abstract class BaseController extends Controller
{
    public $auth;

    public function __construct()
    {
        $this->auth = auth('frontend');
    }
}
