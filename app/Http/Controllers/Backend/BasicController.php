<?php


namespace App\Http\Controllers\Backend;

use App\Constants\BackendConstant;
use App\Constants\SystemConstant;
use App\Exceptions\ServiceException;
use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\AdminLoginPost;
use App\Http\Requests\Backend\AdminUpdateInfoRequest;
use App\Models\Admin;
use App\Services\Base\CommonService;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Request;

class BasicController extends Controller
{
    public function info()
    {
        $admin = Admin::query()->findOrFail(Auth::id());

        $permissions = $admin->getAllPermissions();

        $menus = $permissions->filter(function ($value, $key) {
            return $value->type === 1;
        })->values()->all();

        $buttons = $permissions->filter(function ($value, $key) {
            return $value->type === 2;
        });


        $data[] = [
            // 菜单唯一标示
            'permissionId' => '',
            // 菜单名称
            'name' => '',
            // 页面操作项 例如: 新增 编辑 删除
            'actionEntitySet' => []
        ];

        $return_data = [
            'id' => $admin->id,
            'nickname' => $admin->nickname,
            'avatar' => $admin->avatar,
            'roles' => $admin->getRoleNames(),
            'role' => [
                'menus' => $menus,
                'buttons' => $buttons->pluck('name'),
            ],
        ];

//        $return_data = [
//            'user_info' => [
//                'id' => $admin->id,
//                'nickname' => $admin->nickname,
//                'username' => $admin->username,
//            ],
//            'roles' => [
//                'isSuper' => $admin->is_super,
//                'permissions' => [],
//            ],
//        ];

        return $this->successData($return_data);
    }

    public function updateInfo(AdminUpdateInfoRequest $adminUpdateInfoRequest)
    {
        [
            'nickname' => $nickname,
            'old_password' => $old_password,
            'password' => $password,
        ] = $adminUpdateInfoRequest->fillData();
        $admin = Admin::findOrFail(Auth::id());
        if ($password) {
            if (!Hash::check($old_password, $admin->password)) {
                throw new ServiceException(SystemConstant::WRONG_PASSWORD);
            }
            $admin->password = bcrypt($password);
        }
        $admin->nickname = $nickname;
        $admin->saveOrFail();
        return $this->success();
    }
}
