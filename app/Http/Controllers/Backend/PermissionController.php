<?php
/**
 * laravel-backend
 *
 * @ClassName PermissionController
 * @Author shayvmo
 * @Date 2021-06-20 16:54 星期日
 * @Version 1.0
 * @Description
 */


namespace App\Http\Controllers\Backend;


use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\PermissionPostRequest;
use App\Models\Role;
use App\Services\Base\CommonService;
use App\Models\Permission;

class PermissionController extends Controller
{

    public function index()
    {
        $permissions = Permission::query()->orderByDesc('sort')->get()->toArray();
        $permissions_tree = get_data_tree($permissions);
        return $this->successData(compact('permissions_tree', 'permissions'));
    }


    public function store(PermissionPostRequest $permissionPostRequest)
    {
        $params = $permissionPostRequest->fillData();
        $rules = [
            'name' => [
                'required',
                'string',
                'max: 100',
                'unique:permissions,name'
            ],
        ];
        CommonService::validate($params, $rules);
        $permission = Permission::create($params);
        $permission->path = ($permission->pid > 0 ? $permission->parent->path ?? '0' : '0').'-'.$permission->id;
        $permission->save();
        Role::findByName('super-admin')->givePermissionTo($permission);
        return $this->successData($permission->toArray());
    }


    public function show(Permission $permission)
    {
        return $this->successData($permission->toArray());
    }

    /**
     * @Description
     * @Author Eric
     * @Date 2021/6/24 22:42
     * @param Permission $permission
     * @param PermissionPostRequest $permissionPostRequest
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Exceptions\ValidateException
     * @throws \Throwable
     */
    public function update(Permission $permission, PermissionPostRequest $permissionPostRequest)
    {
        $params = $permissionPostRequest->fillData();
        if ($params['name'] !== $permission->name) {
            $rules = [
                'name' => [
                    'required',
                    'string',
                    'max: 100',
                    'unique:permissions,name'
                ],
            ];
            CommonService::validate($params, $rules);
        }
        $permission->fill($params)->saveOrFail();
        $permission->path = ($permission->pid > 0 ? $permission->parent->path : '0').'-'.$permission->id;
        $permission->save();
        return $this->success();
    }


    public function destroy(Permission $permission)
    {
        $parent_permission_ids = Permission::query()
            ->where('path', 'like', $permission->path.'%')
            ->pluck('id');
        Permission::destroy($parent_permission_ids);
        return $this->success();
    }
}
