<?php
/**
 *
 * @ClassName ArticleCategoryController
 * @Version 1.0
 * @Description
 */


namespace App\Http\Controllers\Backend;


use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Article\ArticleCategoryPostRequest;
use App\Models\ArticleCategory;

class ArticleCategoryController extends Controller
{
    public function index()
    {
        $list = ArticleCategory::query()
            ->orderByDesc('sort')
            ->get()
            ->toArray();
        return $this->successData(compact('list'));
    }

    public function store(ArticleCategoryPostRequest $request)
    {
        $params = $request->fillData();
        ArticleCategory::create($params);
        return $this->success();
    }

    public function show()
    {
        return $this->success('开发中');
    }

    public function update(ArticleCategory $article_category, ArticleCategoryPostRequest $request)
    {
        $params = $request->fillData();
        $article_category->fill($params)->saveOrFail();
        return $this->success();
    }

    public function destroy(ArticleCategory $article_category)
    {
        $article_category->delete();
        return $this->success();
    }
}
