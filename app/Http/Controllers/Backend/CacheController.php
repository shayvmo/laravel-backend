<?php
/**
 *
 * @ClassName CacheController
 * @Version 1.0
 * @Description
 */


namespace App\Http\Controllers\Backend;


use App\Enums\SettingEnum;
use App\Http\Controllers\Controller;
use App\Services\Base\CacheService;
use Illuminate\Http\Request;

class CacheController extends Controller
{
    protected $service;

    public function __construct(CacheService $service)
    {
        $this->service = $service;
    }

    public function index()
    {
        $items = $this->service->items();
        return $this->successData(compact('items'));
    }

    public function clear(Request $request)
    {
        $keys = $request->post('keys');
        $this->service->clear($keys);
        return $this->success();
    }
}
