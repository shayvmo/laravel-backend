<?php

namespace App\Http\Requests\Backend;

use App\Http\Requests\BaseRequest;

class AdminLoginPost extends BaseRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username'=>[
                [
                    'required',
                    'alpha_dash',
                    'between:5,18'
                ],
            ],
            'password'=>[
                [
                    'required',
                    'alpha_dash',
                    'between:5,18'
                ],
            ],
        ];
    }

    public function fillData()
    {
        return [
            'username' => $this->post('username'),
            'password' => $this->post('password'),
        ];
    }
}
