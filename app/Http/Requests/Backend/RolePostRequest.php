<?php


namespace App\Http\Requests\Backend;

use App\Constants\BackendConstant;
use App\Constants\SystemConstant;
use App\Http\Requests\BaseRequest;

class RolePostRequest extends BaseRequest
{
    public function rules()
    {
        return [
            'name'=>[
                'required',
                'string',
                'max:50',
            ],
            'title'=>[
                'required',
                'string',
                'max:25',
            ],
            'desc'=>[
                'sometimes',
                'string',
                'max:80',
                'nullable'
            ],
        ];
    }

    public function fillData()
    {
        return [
            'name' => $this->input('name'),
            'title' => $this->input('title'),
            'desc' => $this->input('desc'),
            'guard_name' => $this->input('guard_name') ?: BackendConstant::AUTH_GUARD,
        ];
    }
}
