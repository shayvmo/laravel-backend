<?php


namespace App\Http\Requests\Backend;

use App\Http\Requests\BaseRequest;

/**
 * 新增、编辑权限
 * Class RulePostRequest
 * @package App\Http\Requests\Backend
 */
class RulePostRequest extends BaseRequest
{
    public function rules()
    {
        return [
            'name'=>[
                'required',
                'string',
                'max:30',
            ],
            'pid'=>[
                'sometimes',
                'integer',
                'min:0'
            ],
            'value'=>[
                'required',
                'string',
                'max:100',
            ],
            'remark'=>[
                'sometimes',
                'string',
                'nullable',
                'max:255',
            ],
        ];
    }

    public function fillData()
    {
        return [
            'name' => $this->post('name'),
            'pid' => $this->post('pid'),
            'value' => $this->post('value'),
            'remark' => $this->post('remark'),
        ];
    }
}
