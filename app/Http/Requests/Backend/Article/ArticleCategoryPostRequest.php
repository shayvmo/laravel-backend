<?php
/**
 *
 * @ClassName ArticleCategoryPostRequest
 * @Version 1.0
 * @Description
 */


namespace App\Http\Requests\Backend\Article;


use App\Http\Requests\BaseRequest;

class ArticleCategoryPostRequest extends BaseRequest
{
    public function rules()
    {
        return [
            'title'=>[
                'required',
                'string',
                'max:30',
            ],
            'status'=>[
                'required',
                'integer',
                'in:0,1'
            ],
            'sort'=>[
                'required',
                'integer',
                'gte:0'
            ],

        ];
    }

    public function fillData()
    {
        return [
            'title' => $this->input('title'),
            'status' => $this->input('status') ?: 0,
            'sort' => $this->input('sort') ?: 0,
        ];
    }
}
