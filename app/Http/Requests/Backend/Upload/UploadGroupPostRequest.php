<?php
/**
 *
 * @ClassName UploadGroupPostRquest
 * @Version 1.0
 * @Description
 */


namespace App\Http\Requests\Backend\Upload;


use App\Http\Requests\BaseRequest;

class UploadGroupPostRequest extends BaseRequest
{
    public function rules()
    {
        return [
            'title'=>[
                'required',
                'string',
                'max:30',
            ],
            'pid'=>[
                'required',
                'integer',
                'gte:0'
            ],
            'sort'=>[
                'required',
                'integer',
                'gte:0'
            ],

        ];
    }

    public function fillData()
    {
        return [
            'title' => $this->input('title'),
            'pid' => $this->input('pid') ?: 0,
            'sort' => $this->input('sort') ?: 0,
        ];
    }
}
