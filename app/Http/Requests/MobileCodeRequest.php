<?php


namespace App\Http\Requests;


class MobileCodeRequest extends BaseRequest
{
    public function rules()
    {
        return [
            'mobile'=>[
                'required',
                'phone',
            ],
            'scene'=>[
                'required',
                'string',
                'between:1, 20',
            ],
        ];
    }

    public function fillData()
    {
        // TODO: Implement fillData() method.
    }
}
