<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * 校验用户权限中间件
 * Class CheckUserAuth
 * @package App\Http\Middleware
 */
class CheckUserAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $permission = $request->route()->getCompiled()->getStaticPrefix().'-'.$request->method();
        //校验用户权限, swoole监听下，无法使用request->user() 拿到当前登陆用户，fpm可以
        if (!Auth::user()->can($permission)) {
            throw new AccessDeniedHttpException(config('app.debug')?'用户暂无权限: '.$permission:'用户暂无权限');
        }
        return $next($request);
    }
}
