<?php

namespace App\Providers;

use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        // 后台管理员敏感操作事件,暂时弃用
//        'App\Events\Backend\AdminEvent' => [
//            'App\Listeners\Backend\AdminLogs'
//        ],

        // 监听实时SQL语句，暂时弃用
//        'Illuminate\Database\Events\QueryExecuted' => [
//            'App\Listeners\QueryListener'
//        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
