<?php

namespace App\Providers;

use App\Enums\SettingEnum;
use App\Enums\StorageEnum;
use App\Services\Base\ConfigService;
use Freyo\Flysystem\QcloudCOSv5\Adapter;
use Iidestiny\Flysystem\Oss\OssAdapter;
use Illuminate\Support\ServiceProvider;
use League\Flysystem\Filesystem;
use Overtrue\Flysystem\Qiniu\QiniuAdapter;
use Qcloud\Cos\Client;

/**
 * Class OssStorageServiceProvider
 *
 * @author iidestiny <iidestiny@vip.qq.com>
 */
class StorageServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $configService = new ConfigService();

        app('filesystem')->extend('oss', function ($app, $config, $configService) {
            $root = $config['root'] ?? null;
            $buckets = $config['buckets'] ?? [];
            $current_config = $configService->getItem(SettingEnum::STORAGE)['engine'][StorageEnum::ALIYUN];
            $config['access_key'] = $current_config['access_key_id'];
            $config['secret_key'] = $current_config['access_key_secret'];
            $config['endpoint'] = $current_config['domain'];
            $config['bucket'] = $current_config['bucket'];
            $config['isCName'] = true;
            $adapter = new OssAdapter(
                $config['access_key'],
                $config['secret_key'],
                $config['endpoint'],
                $config['bucket'],
                $config['isCName'],
                $root,
                $buckets
            );

            $filesystem = new Filesystem($adapter);

            $filesystem->addPlugin(new \Iidestiny\Flysystem\Oss\Plugins\FileUrl());
            $filesystem->addPlugin(new \Iidestiny\Flysystem\Oss\Plugins\SignUrl());
            $filesystem->addPlugin(new \Iidestiny\Flysystem\Oss\Plugins\TemporaryUrl());
            $filesystem->addPlugin(new \Iidestiny\Flysystem\Oss\Plugins\SignatureConfig());
            $filesystem->addPlugin(new \Iidestiny\Flysystem\Oss\Plugins\SetBucket());
            $filesystem->addPlugin(new \Iidestiny\Flysystem\Oss\Plugins\Verify());
            $filesystem->addPlugin(new \Iidestiny\Flysystem\Oss\Plugins\Kernel());

            return $filesystem;
        });

        app('filesystem')->extend('qiniu', function ($app, $config, $configService) {

            $current_config = $configService->getItem(SettingEnum::STORAGE)['engine'][StorageEnum::QINIU];
            $config['access_key'] = $current_config['access_key'];
            $config['secret_key'] = $current_config['secret_key'];
            $config['domain'] = $current_config['domain'];
            $config['bucket'] = $current_config['bucket'];

            $adapter = new QiniuAdapter(
                $config['access_key'],
                $config['secret_key'],
                $config['bucket'],
                $config['domain']
            );



            $flysystem = new Filesystem($adapter);

            $flysystem->addPlugin(new \Overtrue\Flysystem\Qiniu\Plugins\FetchFile());
            $flysystem->addPlugin(new \Overtrue\Flysystem\Qiniu\Plugins\UploadToken());
            $flysystem->addPlugin(new \Overtrue\Flysystem\Qiniu\Plugins\FileUrl());
            $flysystem->addPlugin(new \Overtrue\Flysystem\Qiniu\Plugins\PrivateDownloadUrl());
            $flysystem->addPlugin(new \Overtrue\Flysystem\Qiniu\Plugins\RefreshFile());

            return $flysystem;
        });

        app('filesystem')->extend('qcloud', function ($app, $config, $configService) {
            $demo_config = [
                'driver' => 'cosv5',
                'region'          => env('COSV5_REGION', 'ap-guangzhou'),
                'credentials'     => [
                    'appId'     => env('COSV5_APP_ID'),
                    'secretId'  => env('COSV5_SECRET_ID'),
                    'secretKey' => env('COSV5_SECRET_KEY'),
                    'token'     => env('COSV5_TOKEN'),
                ],
                'timeout'         => env('COSV5_TIMEOUT', 60),
                'connect_timeout' => env('COSV5_CONNECT_TIMEOUT', 60),
                'bucket'          => env('COSV5_BUCKET'),
                'cdn'             => env('COSV5_CDN'),
                'scheme'          => env('COSV5_SCHEME', 'https'),
                'read_from_cdn'   => env('COSV5_READ_FROM_CDN', false),
                'cdn_key'         => env('COSV5_CDN_KEY'),
                'encrypt'         => env('COSV5_ENCRYPT', false),
            ];

            $current_config = $configService->getItem(SettingEnum::STORAGE)['engine'][StorageEnum::QCLOUD];
            $config['domain'] = $current_config['domain'];
            $config['bucket'] = $current_config['bucket'];
            $config['region'] = $current_config['region'];
            $config['credentials']['secretId'] = $current_config['secret_id'];
            $config['credentials']['secretKey'] = $current_config['secret_key'];


            $client = new Client($config);
            $flysystem = new Filesystem(new Adapter($client, $config), $config);

            $flysystem->addPlugin(new \Freyo\Flysystem\QcloudCOSv5\Plugins\PutRemoteFile());
            $flysystem->addPlugin(new \Freyo\Flysystem\QcloudCOSv5\Plugins\PutRemoteFileAs());
            $flysystem->addPlugin(new \Freyo\Flysystem\QcloudCOSv5\Plugins\GetUrl());
            $flysystem->addPlugin(new \Freyo\Flysystem\QcloudCOSv5\Plugins\CDN());
            $flysystem->addPlugin(new \Freyo\Flysystem\QcloudCOSv5\Plugins\TCaptcha());
            $flysystem->addPlugin(new \Freyo\Flysystem\QcloudCOSv5\Plugins\GetFederationToken());
            $flysystem->addPlugin(new \Freyo\Flysystem\QcloudCOSv5\Plugins\GetFederationTokenV3());
            $flysystem->addPlugin(new \Freyo\Flysystem\QcloudCOSv5\Plugins\CloudInfinite());

            return $flysystem;
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
