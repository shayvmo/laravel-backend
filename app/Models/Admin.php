<?php

namespace App\Models;

use App\Constants\BackendConstant;
use App\Traits\ModelSoftDeleteTimestampTrait;
use App\Traits\ModelTimestampTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;
use Tymon\JWTAuth\Contracts\JWTSubject;

/**
 * @property int $id
 * @property string $username  登录账号
 * @property string $password  密码
 * @property string $api_token  api_token
 * @property string $nickname  呢称
 * @property string $avatar  头像
 * @property string $email  邮箱
 * @property int $is_super  是否超级管理员 1 是 0 否
 * @property int $status  1启用 0停用
 * @property string $mobile  手机号
 * @property string $last_login_ip  上次登录IP
 * @property string $last_login_at  上次登录时间
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 */
class Admin extends Authenticatable implements JWTSubject
{
    use Notifiable, SoftDeletes, HasRoles, ModelTimestampTrait, ModelSoftDeleteTimestampTrait;

    protected $table = 'admins';

    protected $guard_name = BackendConstant::AUTH_GUARD;


    /**
     * @var array
     */
    protected $fillable = [
        'username',
        'password',
        'api_token',
        'nickname',
        'avatar',
        'email',
        'is_super',
        'status',
        'mobile',
        'last_login_ip',
        'last_login_at',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'api_token',
    ];

    protected $dispatchesEvents = [
    ];

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }

    public function isValid(): bool
    {
        return (int) $this->status === 1;
    }

    public function isSuper(): bool
    {
        return (int) $this->is_super === 1;
    }

    public function setLastLoginIpAttribute($value)
    {
        $this->attributes['last_login_ip'] = ip2long($value);
    }

    public function getLastLoginIpAttribute($value)
    {
        return long2ip($value);
    }
}
