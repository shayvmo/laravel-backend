<?php

namespace App\Models;

use App\Constants\SystemConstant;
use Illuminate\Support\Carbon;

/**
 * @property int $id
 * @property string $name
 * @property string $guard_name
 * @property string $title  角色名称
 * @property string $desc  描述
 * @property int $system_status  系统保留，1 是 0 否
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 */
class Role extends \Spatie\Permission\Models\Role
{
    public function isSystem(): bool
    {
        return (int) $this->system_status === 1;
    }

    public function getPerPage()
    {
        return SystemConstant::DEFAULT_PAGE_SIZE;
    }

    public function getCreatedAtAttribute($value)
    {
        $value && $value = Carbon::createFromTimestamp(strtotime($value))->toDateTimeString();
        return $value;
    }

    public function getUpdatedAtAttribute($value)
    {
        $value && $value = Carbon::createFromTimestamp(strtotime($value))->toDateTimeString();
        return $value;
    }
}
