<?php

namespace App\Models;

use App\Constants\SystemConstant;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\ModelTimestampTrait;
use App\Traits\ModelSoftDeleteTimestampTrait;

abstract class BaseModel extends Model
{
    use SoftDeletes, ModelTimestampTrait, ModelSoftDeleteTimestampTrait;

    public function getPerPage()
    {
        return SystemConstant::DEFAULT_PAGE_SIZE;
    }

}
