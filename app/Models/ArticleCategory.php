<?php
/**
 *
 * @ClassName ArticleCategory
 * @Version 1.0
 * @Description
 */


namespace App\Models;


class ArticleCategory extends BaseModel
{
    protected $table = 'article_categories';

    /**
     * @var array
     */
    protected $fillable = [
        'title',
        'status',
        'sort',
    ];
}
