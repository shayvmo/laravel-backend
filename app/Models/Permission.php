<?php
/**
 * laravel-backend
 *
 * @ClassName Permission
 * @Author Eric
 * @Date 2021-06-23 18:07 星期三
 * @Version 1.0
 * @Description
 */


namespace App\Models;

use Carbon\Carbon;

/**
 * laravel-backend
 *
 * @ClassName Permission
 * @Author Eric
 * @Date 2021-06-24 22:57 星期四
 * @Version 1.0
 * @Description
 * @package App\Models
 *
 * @property   int  $id
 * @property   int  $pid  父级ID
 * @property   string  $name
 * @property   string  $guard_name
 * @property   string  $path  上下级链
 * @property   string  $sign  sign值
 * @property   string  $title  名称title
 * @property   string  $icon  菜单icon
 * @property   int  $type  权限类型：1 菜单 2 按钮 3 接口
 * @property   string  $route  请求路由
 * @property   string  $method  请求类型： GET,PUT等
 * @property   int  $sort  排序，倒序
 * @property   Carbon  $created_at
 * @property   Carbon  $updated_at
 */
class Permission extends \Spatie\Permission\Models\Permission
{

    public function parent()
    {
        return $this->hasOne(__CLASS__, 'id', 'pid');
    }
}
