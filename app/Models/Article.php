<?php
/**
 *
 * @ClassName Article
 * @Version 1.0
 * @Description
 */


namespace App\Models;


class Article extends BaseModel
{
    protected $table = 'article_articles';

    /**
     * @var array
     */
    protected $fillable = [
        'category_id',
        'user_id',
        'channel',
        'image_id',
        'title',
        'content',
        'is_top',
        'status',
        'sort',
        'virtual_views',
    ];
}
