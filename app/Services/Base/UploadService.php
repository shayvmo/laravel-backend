<?php


namespace App\Services\Base;


use App\Enums\FileChannelEnum;
use App\Enums\FileTypeEnum;
use App\Enums\SettingEnum;
use App\Enums\StorageEnum;
use App\Exceptions\ServiceException;
use App\Models\UploadFile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class UploadService
{
    protected $config;

    /** @var \Illuminate\Contracts\Filesystem\Filesystem  */
    private $disk;

    public function __construct()
    {
        $config = (new ConfigService())->getItem(SettingEnum::STORAGE);
        $this->disk = Storage::disk(StorageEnum::getDisk($config['default']));
        $this->config = config('my.upload');
        $this->config['file_size'] *= (1024*1024);
        $this->config['storage'] = StorageEnum::getDisk($config['default']);
        $this->config['save_storage'] = $config['default'];
        $this->config['engine'] = $config['engine'][$config['default']] ?? [];
    }

    public function uploadFile(Request $request, array $config = []): array
    {
        $config = array_merge($this->config, $config);

        $file = $request->file($config['file_index']);
        if (empty($file)) {
            throw new ServiceException("请使用正确名称上传文件");
        }

        if (!$file->isValid()) {
            throw new ServiceException($file->getClientOriginalName() . ' 无效的上传文件');
        }

        if ($file->getSize() > $config['file_size']) {
            throw new ServiceException($file->getClientOriginalName() . ' 文件大小超出限制');
        }

        if (!in_array($file->getClientOriginalExtension(), $config['file_ext'], true)) {
            throw new ServiceException('文件后缀限制: ' . $file->getClientOriginalExtension());
        }

        if (!in_array($file->getClientMimeType(), $config['file_mime'], true)) {
            throw new ServiceException('未允许的文件mime类型: ' . $file->getClientMimeType());
        }

        $temp_asset = [
            'group_id' => 0,
            'channel' => FileChannelEnum::ADMIN,// 上传来源，枚举
            'storage' => $this->config['save_storage'],// 存储方式，枚举
            'domain' => $this->config['engine']['domain'] ?? Storage::url(''),// 存储域名
            'file_type' => FileTypeEnum::IMAGE,// 文件类型，枚举
            'file_name' => $file->getClientOriginalName(),
            'file_path' => '',
            'file_ext' => $file->getClientOriginalExtension(),
            'file_size' => $file->getSize(),
            'file_md5' => md5_file($file),
//            'uploader_id' => 0,// 上传者ID
        ];

        $file_name = md5_file($file);
        $full_file_path = $file_name . '.' . $file->getClientOriginalExtension();
        if ($asset = UploadFile::query()->where('file_md5', $file_name)->first()) {
            return $asset->toArray();
        }
        $temp_asset['file_path'] = $full_file_path;
        if ($this->disk->has($full_file_path)) {
            return $this->saveAsset($temp_asset);
        }
        $this->disk->putFileAs(
            '',
            $file,
            $full_file_path
        );

        return $this->saveAsset($temp_asset);
    }

    public function uploadImage(Request $request): string
    {
        return $this->uploadFile($request, [
            'file_ext' => [
                'jpg',
                'jpeg',
                'png',
                'gif',
            ],

            'file_mime' => [
                'image/jpeg',
                'image/png',
                'image/gif',
            ],
        ]);
    }

    private function saveAsset(array $asset): array
    {
        [
            'file_md5' => $file_md5
        ] = $asset;
        return UploadFile::query()->whereNotExists(function($query) use ($file_md5) {
            return $query->where('file_md5', $file_md5);
        })->create($asset)->toArray();
    }
}
