<?php

Route::group(['namespace' => 'Backend', 'middleware' => 'request.log', ], function () {
//
    Route::any('test', 'TestController@test');//调试接口

    Route::post('passport/login', 'PassportController@login');// 新登录

    Route::post('passport/logout', 'PassportController@logout');// 退出登录

    // 必须登录后请求
    Route::group(['middleware' => [
        'jwt.token.refresh:backend',
        'auth:backend',
    ]], function () {

        Route::get('user/info', 'BasicController@info');//当前登录用户信息

        Route::put('updateInfo', 'BasicController@updateInfo');//更新用户信息



        // 权限
        Route::group(['middleware' => 'check.user.auth'], function(){
            Route::apiResource('admin', 'AdminsController');//管理员相关
            Route::group(['prefix' => 'admin'], function () {
                Route::put('forbidden/{admin}', 'AdminsController@expressForbidden');//快速禁用
                Route::put('resetPassword/{admin}', 'AdminsController@resetPassword');//重置密码
            });

            Route::apiResource('role', 'RolesController');//角色相关
            Route::group(['prefix' => 'role'], function () {
                Route::get('permission/{role}', 'RolesController@getRolePermissions');//获取角色权限
                Route::put('permission/{role}', 'RolesController@setRolePermissions');//设置角色权限
            });

            Route::apiResource('permission', 'PermissionController');// 统一权限相关

            Route::apiResource('config', 'ConfigController')->only([
                'show','update', 'destroy'
            ]);//配置项

            Route::apiResource('cache', 'CacheController')->only([
                'index'
            ]);// 缓存清理
            Route::group(['prefix' => 'cache'], function () {
                Route::post('clear', 'CacheController@clear');//清除缓存
            });

            // 文件分组
            Route::apiResource('file-group', 'UploadGroupController');
            // 文件列表
            Route::apiResource('file', 'UploadFileController');

            // 文章分类
            Route::apiResource('article-category', 'ArticleCategoryController');
        });
    });

});
