<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(\App\Models\User::class, function (Faker $faker) {

    return [
        'parent_id' => 0,
        'weixin_unionid' => null,
        'weixin_xcx_openid' => $faker->md5,
        'weixin_web_openid' => null,
        'password' => bcrypt('123456'),
        'nickname' => $faker->name,
        'mobile' => get_mock_mobile(),
        'avatar' => 'http://oss.shayvmo.cn/source/sample.jpg',
    ];
});
