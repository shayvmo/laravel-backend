<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUploadGroupTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $table_name = 'upload_groups';
        Schema::create($table_name, function (Blueprint $table) {
            $table->increments('id');
            $table->string('title', 30)->default('')->comment('名称');
            $table->unsignedInteger('pid')->default(0)->comment('父级ID');
            $table->unsignedInteger('sort')->default(0)->comment('倒序');
            $table->timestamps();
            $table->softDeletes();
        });
        DB::statement("ALTER TABLE `".env('DB_PREFIX','')."{$table_name}` comment '文件库分组记录表'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('upload_group');
    }
}
