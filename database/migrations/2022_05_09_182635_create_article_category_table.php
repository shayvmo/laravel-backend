<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArticleCategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $table_name = 'article_categories';
        Schema::create($table_name, function (Blueprint $table) {
            $table->increments('id');
            $table->string('title', 30)->default('')->comment('名称');
            $table->unsignedInteger('status')->default(1)->comment('状态(1显示 0隐藏)');
            $table->unsignedInteger('sort')->default(0)->comment('倒序');
            $table->timestamps();
            $table->softDeletes();
        });
        DB::statement("ALTER TABLE `".env('DB_PREFIX','')."{$table_name}` comment '文章分类表'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('article_category');
    }
}
