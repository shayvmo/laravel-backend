<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUploadFileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $table_name = 'upload_files';
        Schema::create($table_name, function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('group_id')->default(0)->comment('文件分组ID');
            $table->unsignedTinyInteger('channel')->default(0)->comment('0默认，上传来源, 1 后台端 2 用户端');
            $table->string('storage', 10)->default('')->comment('存储方式');
            $table->string('domain')->default('')->comment('存储域名');
            $table->unsignedTinyInteger('file_type')->default(1)->comment('文件类型(1图片 2附件 3视频)');
            $table->string('file_name')->default('')->comment('文件名称(仅显示)');
            $table->string('file_path')->default('')->comment('文件路径');
            $table->string('file_ext', 20)->default('')->comment('文件扩展名');
            $table->string('cover')->default('')->comment('文件封面');
            $table->unsignedInteger('file_size')->default(0)->comment('文件大小(字节)');
            $table->char('file_md5', 32)->default('')->comment('文件MD5');
            $table->unsignedInteger('uploader_id')->default(0)->comment('上传者用户ID');
            $table->unsignedInteger('is_recycle')->default(0)->comment('是否在回收站 1 是 0 否');
            $table->timestamps();
            $table->softDeletes();
        });
        DB::statement("ALTER TABLE `".env('DB_PREFIX','')."{$table_name}` comment '文件库记录表'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('upload_file');
    }
}
