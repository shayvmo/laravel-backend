<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConfigsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $table_name = 'configs';
        Schema::create($table_name, function (Blueprint $table) {
            $table->string('key',30)->nullable(false)->comment('配置项key值');
            $table->string('desc',200)->nullable()->default('')->comment('配置项描述');
            $table->text('values')->nullable(false)->comment('配置项value,json格式');
            $table->timestamps();
            $table->unique('key');
        });
        DB::statement("ALTER TABLE `".env('DB_PREFIX','')."{$table_name}` comment '配置表'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('configs');
    }
}
