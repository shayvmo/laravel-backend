<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArticleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $table_name = 'articles';
        Schema::create($table_name, function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('category_id')->default(0)->comment('文章分类ID');
            $table->unsignedInteger('user_id')->default(0)->comment('用户ID');
            $table->unsignedTinyInteger('channel')->default(0)->comment('0默认，来源, 1 后台端 2 用户端');
            $table->unsignedInteger('image_id')->default(0)->comment('封面图ID');
            $table->string('title')->default('')->comment('文章标题');
            $table->longText('content')->nullable(false)->comment('文章内容');
            $table->unsignedTinyInteger('is_top')->default(0)->comment('是否置顶，1是 0 否');
            $table->unsignedTinyInteger('status')->default(0)->comment('文章状态(0隐藏 1显示)');
            $table->unsignedInteger('sort')->default(0)->comment('倒排');
            $table->unsignedInteger('virtual_views')->default(0)->comment('虚拟阅读量(仅用作展示)');
            $table->unsignedInteger('actual_views')->default(0)->comment('实际阅读量');
            $table->timestamps();
            $table->softDeletes();
            $table->index(['category_id', 'is_top']);
        });
        DB::statement("ALTER TABLE `".env('DB_PREFIX','')."{$table_name}` comment '文章记录表'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('article');
    }
}
