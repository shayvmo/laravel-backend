# laravel-backend

## ！！！ 已暂停维护 ！！！

坚持不下来，就此暂停。

---



#### 介绍
基于laravel的后台基础管理系统，采用前后端分离方式，前端页面使用vue-element-admin，后端API使用laravel

#### 软件架构
laravel 作为后端API提供框架，实行前后端分离

后台管理页面前端仓库：[https://gitee.com/shayvmo/vue-elemnt-admin](https://gitee.com/shayvmo/vue-elemnt-admin)

#### 安装教程

【配置伪静态】
```
location / {
    try_files $uri $uri/ /index.php?$query_string;
}
```

1、安装依赖的composer包
```shell script
composer install
```

2、访问` http://域名/install `，即可设置数据库地址。

设置完成后，运行上述2个步骤即可。

【注】

1、本地开发部署时，安装成功后，可以直接登录访问后台，默认 admin 123456

2、清除安装锁命令: ` php artisan install:clean `

3、手动清除 ` permission ` 缓存: `php artisan cache:forget spatie.permission.cache`

#### 使用说明

暂无

#### 规划功能

- 配置项管理

- 刷新权限，增加新权限
    
- 本地日志查询

- 其他完善



#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

代码提交备注说明

- `!` 修复BUG

- `+` 新增功能

- `*` 普通修改

例子：

`[!] 修复BUG` 

`[+] 新增功能`

`[*] 修改xxx`


#### 其他

swoole

[https://github.com/hhxsv5/laravel-s](https://github.com/hhxsv5/laravel-s)

```php
// 项目根目录执行，启动swoole监听
php bin/laravels start
```
