<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted' => 'The :attribute must be accepted.',
    'active_url' => ' :attribute 不是一个合法的 URL.',
    'after' => 'The :attribute must be a date after :date.',
    'after_or_equal' => 'The :attribute must be a date after or equal to :date.',
    'alpha' => ' :attribute 只能由字母组成',
    'alpha_dash' => 'The :attribute may only contain letters, numbers, and dashes.',
    'alpha_num' => ' :attribute 只能由字母、数字组成',
    'array' => ' :attribute 必须是数组',
    'before' => 'The :attribute must be a date before :date.',
    'before_or_equal' => 'The :attribute must be a date before or equal to :date.',
    'between' => [
        'numeric' => ' :attribute 必须在 :min 和 :max 之间',
        'file' => 'The :attribute must be between :min and :max kilobytes.',
        'string' => ' :attribute 长度必须在 :min 和 :max 之间',
        'array' => 'The :attribute must have between :min and :max items.',
    ],
    'boolean' => ' :attribute 只能是布尔 true or false',
    'confirmed' => ' :attribute 确认不匹配',
    'date' => ' :attribute 不是合法的日期时间',
    'date_equals' => 'The :attribute must be a date equal to :date.',
    'date_format' => ' :attribute 格式不匹配 :format',
    'different' => ' :attribute 和 :other 不能相同',
    'digits' => 'The :attribute must be :digits digits.',
    'digits_between' => 'The :attribute must be between :min and :max digits.',
    'dimensions' => 'The :attribute has invalid image dimensions.',
    'distinct' => 'The :attribute field has a duplicate value.',
    'email' => ' :attribute 必须是一个合法的邮箱',
    'ends_with' => 'The :attribute must end with one of the following: :values',
    'exists' => 'The selected :attribute is invalid.',
    'file' => ' :attribute 必须是文件',
    'filled' => ' :attribute 必须传值',
    'gt' => [
        'numeric' => 'The :attribute must be greater than :value.',
        'file' => 'The :attribute must be greater than :value kilobytes.',
        'string' => 'The :attribute must be greater than :value characters.',
        'array' => 'The :attribute must have more than :value items.',
    ],
    'gte' => [
        'numeric' => 'The :attribute must be greater than or equal :value.',
        'file' => 'The :attribute must be greater than or equal :value kilobytes.',
        'string' => 'The :attribute must be greater than or equal :value characters.',
        'array' => 'The :attribute must have :value items or more.',
    ],
    'image' => ' :attribute 必须是图片',
    'in' => ' :attribute 值不可用',
    'in_array' => ' :attribute 必须在 :other 里面',
    'integer' => ' :attribute 必须是 整数',
    'ip' => ' :attribute 必须是合法的 IP 地址',
    'ipv4' => ' :attribute 必须是合法的 IPv4 地址',
    'ipv6' => ' :attribute 必须是合法的 IPv6 地址',
    'json' => ' :attribute 必须是合法的 JSON 字符串',
    'lt' => [
        'numeric' => 'The :attribute must be less than :value.',
        'file' => 'The :attribute must be less than :value kilobytes.',
        'string' => 'The :attribute must be less than :value characters.',
        'array' => 'The :attribute must have less than :value items.',
    ],
    'lte' => [
        'numeric' => 'The :attribute must be less than or equal :value.',
        'file' => 'The :attribute must be less than or equal :value kilobytes.',
        'string' => 'The :attribute must be less than or equal :value characters.',
        'array' => 'The :attribute must not have more than :value items.',
    ],
    'max' => [
        'numeric' => ' :attribute 不能大于 :max.',
        'file' => ' :attribute 不能大于 :max kb',
        'string' => ' :attribute 长度不能大于 :max 个字符',
        'array' => ' :attribute 元素个数不能大于 :max ',
    ],
    'mimes' => ' :attribute 文件类型必须是: :values ',
    'mimetypes' => ' :attribute 文件类型必须是: :values ',
    'min' => [
        'numeric' => ' :attribute 必须大于等于 :min.',
        'file' => ' :attribute 最少 :min kilobytes.',
        'string' => ' :attribute 必须大于等于 :min 个字符',
        'array' => ' :attribute 至少包含 :min 个元素',
    ],
    'not_in' => ' :attribute 值不可用',
    'not_regex' => 'The :attribute format is invalid.',
    'numeric' => ' :attribute 必须是数字',
    'present' => 'The :attribute field must be present.',
    'regex' => ' :attribute 参数格式错误',
    'required' => ' :attribute 参数必填 ',
    'required_if' => '当 :other 值为 :value 时，:attribute 必填',
    'required_unless' => 'The :attribute field is required unless :other is in :values.',
    'required_with' => 'The :attribute field is required when :values is present.',
    'required_with_all' => 'The :attribute field is required when :values is present.',
    'required_without' => 'The :attribute field is required when :values is not present.',
    'required_without_all' => 'The :attribute field is required when none of :values are present.',
    'same' => 'The :attribute and :other must match.',
    'size' => [
        'numeric' => 'The :attribute must be :size.',
        'file' => 'The :attribute must be :size kilobytes.',
        'string' => 'The :attribute must be :size characters.',
        'array' => 'The :attribute must contain :size items.',
    ],
    'starts_with' => 'The :attribute must start with one of the following: :values',
    'string' => ' :attribute 必须是字符串',
    'timezone' => 'The :attribute must be a valid zone.',
    'unique' => ' :attribute 值已被占用,请修改',
    'uploaded' => 'The :attribute failed to upload.',
    'url' => ' :attribute 格式不合法',
    'uuid' => ' :attribute 必须是一个合法的 UUID.',

    'chinese' => ':attribute 不能含有文字',
    'phone' => '请输入正确的手机号',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap our attribute placeholder
    | with something more reader friendly such as "E-Mail Address" instead
    | of "email". This simply helps us make our message more expressive.
    |
    | 验证失败时，转换验证的属性名称为中文，再进行返回提示
    */
    'attributes' => [
        'username' => '账号',
        'email' => '邮箱地址',
        'mobile' => '手机号',
        'sms_code' => '验证码',
        'nickname' => '呢称',
        'old_password' => '密码',
        'password' => '密码',
        'password_confirmation' => '确认密码',
        'user_note' => '备注',
    ],

];
