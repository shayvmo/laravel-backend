<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>安装页面</title>

        <style>
            #app {
                width: 1000px;
                margin: 0 auto;
            }
        </style>
        <script src="https://unpkg.com/vue@2.6.12/dist/vue.js"></script>
        <script src="https://unpkg.com/axios@0.21.1/dist/axios.min.js"></script>
        <!-- 引入样式 -->
        <link rel="stylesheet" href="https://unpkg.com/element-ui@2.15.1/lib/theme-chalk/index.css">
        <!-- 引入组件库 -->
        <script src="https://unpkg.com/element-ui@2.15.1/lib/index.js"></script>
    </head>
    <body>
        <div id="app">
            <el-row>
                <el-col :span="24">
                    <div class="grid-content bg-purple-dark">
                        <el-form ref="form" :model="form" label-width="150px" :rules="rules">
                            <el-form-item label="数据库地址" prop="db_host">
                                <el-input v-model="form.db_host" ></el-input>
                            </el-form-item>
                            <el-form-item label="端口号" prop="db_port">
                                <el-input v-model="form.db_port"></el-input>
                            </el-form-item>
                            <el-form-item label="数据库名称" prop="db_database">
                                <el-input v-model="form.db_database"></el-input>
                            </el-form-item>
                            <el-form-item label="用户名" prop="db_username">
                                <el-input v-model="form.db_username"></el-input>
                            </el-form-item>
                            <el-form-item label="密码" prop="db_password">
                                <el-input v-model="form.db_password"></el-input>
                            </el-form-item>
                            <el-form-item label="数据库表前缀" prop="db_prefix">
                                <el-input v-model="form.db_prefix"></el-input>
                            </el-form-item>
                            <el-form-item>
                                <el-button type="primary" @click="onSubmit" :loading="loading">立即安装</el-button>
                                <el-button>取消</el-button>
                            </el-form-item>
                        </el-form>
                    </div>
                </el-col>

            </el-row>
        </div>
    </body>
    <script>
        const app = new Vue({
            el: '#app',
            data: {
                form: {
                    db_host: '{{ $env_array['DB_HOST'] ?? ''}}',
                    db_port: '{{ $env_array['DB_PORT']  ?? '3306'}}',
                    db_database: '{{ $env_array['DB_DATABASE']  ?? ''}}',
                    db_username: '{{ $env_array['DB_USERNAME']  ?? ''}}',
                    db_password: '{{ $env_array['DB_PASSWORD']  ?? ''}}',
                    db_prefix: '{{ $env_array['DB_PREFIX']  ?? ''}}',
                },
                rules: {
                    db_host: [
                        { required: true, message: '请输入数据库地址', trigger: 'blur' }
                    ],
                    db_port: [
                        { required: true, message: '请输入端口', trigger: 'blur' }
                    ],
                    db_database: [
                        { required: true, message: '请输入数据库名称', trigger: 'blur' }
                    ],
                    db_username: [
                        { required: true, message: '请输入账号', trigger: 'blur' }
                    ],
                    db_password: [
                        { required: true, message: '请输入密码', trigger: 'blur' }
                    ],
                    db_prefix: [
                        { min: 0, max: 10, message: '长度不能超过 10 个字符', trigger: 'blur' }
                    ],
                },
                loading: false
            },
            methods: {
                onSubmit() {
                    this.$refs['form'].validate((valid) => {
                        if (valid) {
                            let that = this
                            that.loading = true
                            axios.post('install/save', this.form).then(function(response) {
                                that.loading = false
                                if (response.data.code === 0) {
                                    that.$notify.success({
                                        title: '成功',
                                        message: response.data.msg,
                                        duration: 1000,
                                        onClose: function() {
                                            that.$notify.info({
                                                title: '消息',
                                                message: '进入安装程序',
                                                duration: 1000,
                                                onClose: function () {
                                                    that.execute()
                                                }
                                            });

                                        },
                                    });
                                } else {
                                    that.$notify.error({
                                        title: '错误',
                                        message: response.data.msg
                                    });
                                }
                            })
                        } else {
                            console.log('error submit!!');
                            return false;
                        }
                    })
                },
                execute() {
                    let that = this
                    const loading = this.$loading({
                        lock: true,
                        text: '执行安装中',
                        spinner: 'el-icon-loading',
                        background: 'rgba(0, 0, 0, 0.7)'
                    });
                    axios.post('install/execute').then(function(response) {
                        loading.close();
                        if (response.data.code === 0) {
                            that.$notify.success({
                                title: '成功',
                                message: response.data.msg,
                                duration: 1000,
                                onClose: function() {
                                    location.href = '/index.html';
                                },
                            });
                        } else {
                            that.$notify.error({
                                title: '错误',
                                message: response.data.msg
                            });
                        }
                    })
                }
            }
        });
    </script>
</html>
